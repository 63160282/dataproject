/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.jirarat.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;




/**
 *
 * @author werapan
 */
public class Orders {

    private int id;
    private Date orderDate;
    private double total;
    private int qty;
    private ArrayList<Orders_Detail> orderDetails;

    public Orders(int id, Date orderDate, double total, int qty, ArrayList<Orders_Detail> orderDetails) {
        this.id = id;
        this.orderDate = orderDate;
        this.total = total;
        this.qty = qty;
        this.orderDetails = orderDetails;
    }

    public Orders() {
        this.id = -1;
        orderDetails = new ArrayList<>();
        qty = 0;
        total = 0;
        orderDate = new Date();

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public ArrayList<Orders_Detail> getOrders_Details() {
        return orderDetails;
    }

    public void setOrders_Details(ArrayList<Orders_Detail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    @Override
    public String toString() {
        return "Orders{" + "id=" + id + ", orderDate=" + orderDate + ", total=" + total + ", qty=" + qty + "}";
    }

    public void addOrders_Detail(Orders_Detail orderdetail) {
        orderDetails.add(orderdetail);
        total = total + orderdetail.getTotal();
        qty = qty + orderdetail.getQty();
    }

    public void addOrders_Detail(product product, String productName, double productPrice, int qty) {
        Orders_Detail orderDetail = new Orders_Detail(product, productName, productPrice, qty, this);
        this.addOrders_Detail(orderDetail);
    }

    public void addOrders_Detail(product product, int qty) {
        Orders_Detail orderDetail = new Orders_Detail(product, product.getName(), product.getPrice(), qty, this);
        this.addOrders_Detail(orderDetail);
    }

    public static Orders fromRS(ResultSet rs) {
        Orders order = new Orders();
        try {
            order.setId(rs.getInt("order_id"));
            order.setQty(rs.getInt("order_qty"));
            order.setTotal(rs.getInt("order_total"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            order.setOrderDate(sdf.parse(rs.getString("order_date")));
        } catch (SQLException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
        }
        return order;
    }
}
